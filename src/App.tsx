import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableContainer,
} from '@chakra-ui/react'
import './App.css';
import { ChakraProvider } from '@chakra-ui/react'
import axios from 'axios'
import { useState, useEffect } from 'react'

type Todo = {
  description: string
}

function App() {
  const [todos, setTodos] = useState<Todo[]>([])
  useEffect(() => {
    let url = `${process.env.REACT_APP_API_URL}/api/todos/`
    console.info(url)
    axios.get(url,)
      .then((response) => { setTodos(response.data) })
      .catch((error) => { console.log(error) })
  }, [])
  return (
    <ChakraProvider>
      <TableContainer>
        <Table variant='simple'>
          <Thead>
            <Tr>
              <Th>TODO</Th>
            </Tr>
          </Thead>
          <Tbody>
            {todos.map((todo) => (
              <Tr key={todo.description}>
                <Td>{todo.description}</Td>
              </Tr>
            ))}
            <Tr>
            </Tr>
          </Tbody>
        </Table>
      </TableContainer>
    </ChakraProvider>
  );
}

export default App;
